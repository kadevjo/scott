﻿using UnityEngine;
using System.Collections;

public class SpeedBar : MonoBehaviour {

	public int multiplierAmmount = 0;
	public float speedBarValue = 0;
	public Vector2 multiplierPosition;
	public bool isPaused;

	[SerializeField]
	private float upperMultiplierPosition;
	[SerializeField]
	private float lowerMultiplierPosition;
	[SerializeField]
	private GameObject scrollableObject;
	[SerializeField]
	private GameObject multiplier;
	[SerializeField]
	private GameObject multiplierAnimation;
	[SerializeField]
	private TimerPrefab timer;
	[SerializeField]
	private UISprite thermometerBase;
	[SerializeField]
	private UISprite thermometerBody;

	private UILabel multiplierLabel;
	private UIProgressBar progressBar;

	void Start () {
		progressBar = GetComponent<UIProgressBar> ();
		multiplierLabel = multiplier.GetComponent<UILabel> ();
		timer.onSecondChange += MultiplierAnimationPerSecond;
	}

	void Update () {
		if (!this.isPaused)
			progressBar.value = (progressBar.value + ((int)(scrollableObject.rigidbody.velocity.y * - 0.3f)) / 100f - 0.01f); // Speed Bar Difficult

		speedBarValue = progressBar.value;
		multiplierAmmount = (int)(speedBarValue * 10);
		multiplierLabel.text = "x" + multiplierAmmount.ToString ();

		multiplierPosition = multiplier.transform.localPosition;
		multiplierPosition.y = (upperMultiplierPosition - lowerMultiplierPosition) * speedBarValue + lowerMultiplierPosition;
		multiplier.transform.localPosition = multiplierPosition;

		Color thermometerColor;

		if (speedBarValue < 0.5f) {
			thermometerColor = new Color(1, 0.14f + 0.86f * speedBarValue * 2, 0.14f);
		} else {
			thermometerColor = new Color(1 - 0.86f * (speedBarValue - 0.5f) * 2, 1, 0.14f);
		}

		thermometerBase.color = thermometerColor;
		thermometerBody.color = thermometerColor;
	}

	void MultiplierAnimationPerSecond () {
		Vector2 multiplierAnimationPosition = multiplierAnimation.transform.localPosition;
		multiplierAnimationPosition.y = multiplier.transform.localPosition.y;
		multiplierAnimation.transform.localPosition = multiplierAnimationPosition;

		multiplierAnimation.GetComponent<UILabel> ().text = multiplierLabel.text;
		multiplierAnimation.animation.Play ();
	}
}
