﻿using UnityEngine;
using System.Collections;

public class StripedLine : MonoBehaviour {
	public Vector3 initialPosition;
	public float initialPositionReference;
	public bool isOnScreen;
	public bool isSpecial;

	private UnrollablePaper unrollablePaper;

	public void Init (float posReference, UnrollablePaper unrollablePaper) {
		initialPositionReference = posReference;
		initialPosition = transform.position;
		isOnScreen = true;
		this.unrollablePaper = unrollablePaper;
	}

	void Update () {
		if (isSpecial && isOnScreen) {
			Vector3 positionOnScreen = Camera.main.WorldToScreenPoint (transform.position);

			if (positionOnScreen.y < Screen.height * 0.5f) {
				this.unrollablePaper.OnPaperCut ();
				isOnScreen = false;
			}
		}
	}
}
