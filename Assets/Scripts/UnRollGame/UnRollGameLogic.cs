﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UnRollGameLogic : MonoBehaviour {

	//paper roll lenght difficult factor

	//speed bar difficult factor
	public int currentScore = 0;
	public bool paperRollFinished = false;
	public int distance = 0;

	[SerializeField]
	private UnRollUI unRollUI;
	[SerializeField]
	private PaperDragManager paperDragManager;
	[SerializeField]
	private UnrollablePaper unrollablePaper;
	[SerializeField]
	private GameObject scrollableObject;	
	[SerializeField]
	private GameObject paperRoll; 	
	[SerializeField]
	private SpeedBar speedBar;	
	[SerializeField]
	private TimerPrefab timer;		
	[SerializeField]
	private UISprite circle;	
	[SerializeField]
	private UISprite paperMask;
	[SerializeField]
	private AudioSource music;

	private GameObject paperRollPanel;
	private int totalSeconds = 0;
	private int width;
	private int height;
	private float paperMaskY;
	private List<byte> multipliersPerSecond = new List<byte>();

	const int minimumSeconds = 10;
	const int maximumSeconds = 90;
	const int maximumScoreByTime = 1000;

	// Use this for initialization
	void Start () {
		this.width = circle.width;
		this.height = circle.height;
		this.paperMaskY = paperMask.transform.position.y;
		this.paperRollPanel = paperRoll.transform.GetChild (0).gameObject;
		this.timer.Run ();
		this.timer.onSecondChange += OnSecondChange;
	}

	private void OnSecondChange () {
		multipliersPerSecond.Add ((byte)(speedBar.multiplierAmmount));
		totalSeconds++;
	}

	private void CalculateScore () {
		int scoreByTime = ((maximumSeconds - totalSeconds) * maximumScoreByTime) / (maximumSeconds - minimumSeconds);

		if (scoreByTime < 0) scoreByTime = 0;

		float multiplier = 0;

		foreach (byte ammount in multipliersPerSecond) {
			multiplier += ammount;
		}

		multiplier = Mathf.RoundToInt(multiplier / multipliersPerSecond.Count);
		currentScore = scoreByTime * (int)multiplier;
		currentScore = (int)(currentScore / 10) * unrollablePaper.specialStripesTapped;
	}

	private void EndGame () {
		unrollablePaper.OnGameEnd ();
		paperRollFinished = true;
		CalculateScore ();

		int bestScore = PlayerPrefs.GetInt ("UnRollTopScore");
		bool isNewRecord = false;

		if (currentScore > bestScore) {
			isNewRecord = true;
			bestScore = currentScore;
			PlayerPrefs.SetInt("UnRollTopScore", currentScore);
			PlayerPrefs.Save ();
		}

		EndOfGameWindow.Show ("UnRollGameScene", currentScore, bestScore, isNewRecord);
		EndOfGameWindow.Instance.ShowPack ("UnRollGameScene");
	}

	void Update () {
		paperRollPanel.renderer.material.mainTextureOffset = new Vector2 (0, scrollableObject.transform.position.y);
		
		//Difficulty can be changed here
		distance = (int)(-scrollableObject.transform.position.y * 0.2f);
		float heightPercentage = 1 - (distance/100f);
		float widthPercentage = 1 - (distance/75f);
		float paperRollPercentage = 1 - (distance / 25f);

		circle.SetDimensions ((int)(this.width * widthPercentage), (int)(this.height * heightPercentage));	
		paperMask.transform.position = new Vector2 (paperMask.transform.position.x, (this.paperMaskY) * -distance * 20);

		CalculateScore ();
		unRollUI.SetScore (Mathf.Clamp (currentScore, 0, currentScore));

		if (paperRollFinished){
			return;
		}
		
		if (heightPercentage < 0.59f) {
			paperRoll.rigidbody2D.isKinematic = false;
			timer.Pause();
			
			if (paperRoll.transform.position.y < -3) {
				EndGame ();
			}
		}
	}

	public void PauseGame () {
		timer.Pause ();
		speedBar.isPaused = true;
		paperDragManager.isPaused = true;
		music.volume = 0.5f;
	}

	public void ResumeGame () {
		timer.Resume ();
		speedBar.isPaused = false;
		paperDragManager.isPaused = false;
		music.volume = 1;
	}
}
