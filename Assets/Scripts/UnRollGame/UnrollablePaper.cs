using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class UnrollablePaper : MonoBehaviour {
	[SerializeField]
	private UnRollGameLogic gameManager;
	[SerializeField]
	private GameObject scrollableObject;
	[SerializeField]
	private GameObject mainPaperPiece;
	[SerializeField]
	private GameObject auxPaperPiece;
	[SerializeField]
	private Camera gameCamera;
	[SerializeField]
	private int maxPaperHeigth;
	[SerializeField]
	private List<GameObject> stripedLines;
	[SerializeField]
	private GameObject firstStripedLine;
	[SerializeField]
	private GameObject paperCorner;
	[SerializeField]
	private GameObject paperBody;
	[SerializeField]
	private GameObject stripedLinePrefab;
	[SerializeField]
	private GameObject specialStripedLine;
	[SerializeField]
	private Transform stripedLinesParent;
	[SerializeField]
	private GameObject rollWidthCircle;
	[SerializeField]
	private GameObject secondaryPaperPiece;
	[SerializeField]
	private AudioSource sfx;
	[SerializeField]
	private GameObject specialStripeMultiplier;

	[SerializeField]
	private float globalScrollFactor = 1f;
	[SerializeField]
	private float paperHeightFactor = 100f;
	[SerializeField]
	private float stripedLineFactor = 1f;
	[SerializeField]
	private float rollWidthFactor = 1f; 

	public int specialStripesTapped;

	private bool isFullLength;
	private int initialPaperHeigth;
	private float initialScrollLength;
	private float maxStripedLineDistance;
	private int initialRollHeight, initialRollWidth;
	private bool gameEnd;
	private float lastSpecialStripePosition;
	private float nextSpecialStripePosition;
	private int initialSecondaryHeight;
	private Vector3 initialSecondaryPosition;
	private int stripeLinesNumber = 0;

	void Start () {
		initialSecondaryHeight = secondaryPaperPiece.GetComponent<UISprite>().height;
		initialSecondaryPosition = secondaryPaperPiece.transform.position;

		this.initialRollHeight = rollWidthCircle.GetComponent<UISprite>().height;
		this.initialRollWidth = rollWidthCircle.GetComponent<UISprite>().width;
		this.initialScrollLength = scrollableObject.transform.position.y;
		this.initialPaperHeigth = mainPaperPiece.GetComponent<UISprite>().height;
		maxStripedLineDistance = firstStripedLine.transform.position.y - mainPaperPiece.GetComponent<UISprite>().worldCorners[0].y;
		stripedLines.Add (firstStripedLine);
		firstStripedLine.GetComponent<StripedLine>().Init(scrollableObject.transform.position.y - this.initialScrollLength, this);

		this.lastSpecialStripePosition = 1f;
		this.nextSpecialStripePosition = 1f - Random.Range (0.04f, 0.06f);

		specialStripesTapped = 1;
	}

	float scrollForPaperHeight = 0;

	void Update () {
		float scrollLength = scrollableObject.transform.position.y - this.initialScrollLength;
		float paperBottomOnScreen = gameCamera.WorldToScreenPoint (
			mainPaperPiece.GetComponent<UISprite>().worldCorners[0]
		).y;

		// Check if paper is at its full length
		if (!isFullLength) {
			// 
			mainPaperPiece.GetComponent<UISprite>().SetDimensions (
				mainPaperPiece.GetComponent<UISprite>().width,
				this.initialPaperHeigth + (int)(-(scrollLength - scrollForPaperHeight) * paperHeightFactor * globalScrollFactor)
			);

			this.isFullLength = paperBottomOnScreen < 0;
		}

		int distance = (int)(-scrollableObject.transform.position.y * 0.2f);
		float rollWidthCircleHeight = 1 - (distance/100f) * rollWidthFactor * globalScrollFactor;
		float rollWidthCircleWidth = 1 - (distance/75f) * rollWidthFactor * globalScrollFactor;
		
		rollWidthCircle.GetComponent<UISprite>().SetDimensions (
			(int)(this.initialRollWidth * rollWidthCircleWidth), (int)(this.initialRollHeight * rollWidthCircleHeight)
		);

		if (stripedLines.Count > 0  && !gameEnd) {
			stripedLines.Where (sl => sl.GetComponent<StripedLine>().isOnScreen).ToList ().ForEach (sl => {
				StripedLine stripedLine = sl.GetComponent<StripedLine>();
				Vector3 newPos = sl.transform.position;
				newPos.y = stripedLine.initialPosition.y + (scrollLength - stripedLine.initialPositionReference) * stripedLineFactor * globalScrollFactor;
				newPos.x = stripedLine.initialPosition.x;
				sl.transform.position = newPos;

				Vector3 upperBoundPosition = sl.transform.position;
				upperBoundPosition.y = sl.GetComponent<UISprite>().worldCorners[1].y;

				if (sl.GetComponent<StripedLine>().isSpecial)
					upperBoundPosition.y += sl.GetComponent<UISprite>().worldCorners[1].y - sl.transform.position.y;

				if (gameCamera.WorldToScreenPoint (upperBoundPosition).y < -50) {
					stripedLine.isOnScreen = false;
				}
			});

			float paperTop = paperCorner.GetComponent<UISprite>().worldCorners[0].y;
			float lastStripedLineDistance = paperCorner.GetComponent<UISprite>().worldCorners[1].y - stripedLines[stripedLines.Count - 1].transform.position.y;

			if (lastStripedLineDistance > maxStripedLineDistance) {
				this.stripeLinesNumber++;

				if (this.stripeLinesNumber == 6) {
					specialStripedLine.GetComponent<UISprite>().alpha = 1;
					specialStripedLine.GetComponentsInChildren<UISprite>().ToList ().ForEach (s => {
						s.alpha = 1;
					});

					Vector3 newPos = specialStripedLine.transform.position;
					newPos.y = paperCorner.GetComponent<UISprite>().worldCorners[1].y;
					specialStripedLine.transform.position = newPos;
					specialStripedLine.GetComponent<StripedLine>().Init (scrollableObject.transform.position.y - this.initialScrollLength, this);

					if (stripedLines.Contains (specialStripedLine))
						stripedLines.Remove (specialStripedLine);

					stripedLines.Add (specialStripedLine);
					this.stripeLinesNumber = 0;
				} else {
					if (stripedLines
					    .Where (sl => !sl.GetComponent<StripedLine>().isOnScreen && !sl.GetComponent<StripedLine>().isSpecial)
					    .ToList ()
					    .Count == 0) 
					{
						GameObject stripedLineInstance = Instantiate (stripedLinePrefab) as GameObject;
						stripedLineInstance.transform.parent = stripedLinesParent;
						Vector3 newPos = transform.TransformPoint (stripedLineInstance.transform.position);
						newPos.y = paperCorner.GetComponent<UISprite>().worldCorners[1].y;
						stripedLineInstance.transform.position = newPos;//stripedLineInstance.transform.localPosition = stripedLineInstance.transform.position;
						stripedLineInstance.transform.localScale = Vector3.one;
						stripedLineInstance.GetComponent<StripedLine>().Init(scrollableObject.transform.position.y - this.initialScrollLength, this);
						stripedLines.Add (stripedLineInstance);
					} else {
						GameObject stripedLine = stripedLines
							.Where (sl => !sl.GetComponent<StripedLine>().isOnScreen  && !sl.GetComponent<StripedLine>().isSpecial)
							.ToList ()
							.First ();

						stripedLine.GetComponent<UISprite>().alpha = 1;
						Vector3 newPos = transform.TransformPoint (stripedLinePrefab.transform.position);
						newPos.y = paperCorner.GetComponent<UISprite>().worldCorners[1].y;
						stripedLine.transform.position = newPos;
						stripedLine.GetComponent<StripedLine>().Init (scrollableObject.transform.position.y - this.initialScrollLength, this);
						stripedLines.Remove (stripedLine);
						stripedLines.Add (stripedLine);
					}
				}
			}
		}
	}

	public void OnPaperCut () {
		UISprite mainPaperPieceSprite = mainPaperPiece.GetComponent<UISprite>();
		float specialStripePos = specialStripedLine.transform.position.y;
		int mainPaperPieceHeight = 0;

		mainPaperPieceSprite.SetDimensions (
			mainPaperPieceSprite.width,
			mainPaperPieceHeight
		);

		while(mainPaperPieceSprite.worldCorners[0].y > specialStripePos 
		      && gameCamera.WorldToScreenPoint (mainPaperPieceSprite.worldCorners[0]).y > 0)
		{
			mainPaperPieceHeight += 5;

			mainPaperPieceSprite.SetDimensions (
				mainPaperPieceSprite.width,
				mainPaperPieceHeight
			);
		}

		secondaryPaperPiece.transform.position = initialSecondaryPosition;
		secondaryPaperPiece.transform.rotation = Quaternion.identity;
		secondaryPaperPiece.rigidbody2D.isKinematic = true;

		this.initialPaperHeigth = mainPaperPieceSprite.height;
		scrollForPaperHeight = scrollableObject.transform.position.y - this.initialScrollLength;
		this.isFullLength = gameCamera.WorldToScreenPoint (mainPaperPieceSprite.worldCorners[0]).y < 0;

		UISprite secondaryPaperPieceSprite = secondaryPaperPiece.GetComponent<UISprite>();
		int secondaryPaperPieceHeight = 0;
		
		secondaryPaperPieceSprite.SetDimensions (
			secondaryPaperPieceSprite.width,
			secondaryPaperPieceHeight
			);
		
		while(secondaryPaperPieceSprite.worldCorners[1].y < specialStripePos) {
			secondaryPaperPieceHeight += 5;
			
			secondaryPaperPieceSprite.SetDimensions (
				secondaryPaperPieceSprite.width,
				secondaryPaperPieceHeight
				);
		}

		secondaryPaperPiece.rigidbody2D.isKinematic = false;
		float paperPieceHorizontalForce = Random.Range (30f, 50f) * Mathf.Sign (Random.Range (-1f, 1f));
		secondaryPaperPiece.rigidbody2D.AddForce (new Vector2(paperPieceHorizontalForce, Random.Range (-50f, 0f)));
		secondaryPaperPiece.rigidbody2D.AddTorque (Random.Range (30f, 50f) * Mathf.Sign (Random.Range (-1f, 1f)));

		specialStripedLine.GetComponent<UISprite>().alpha = 0;
		specialStripedLine.GetComponentsInChildren<UISprite>().ToList ().ForEach (s => {
			s.alpha = 0;
		});

		stripedLines
			.Where (sl => sl.GetComponent<StripedLine>().isOnScreen && sl.transform.position.y < specialStripePos)
			.ToList ()
			.ForEach (sl => sl.GetComponent<UISprite>().alpha = 0);

		sfx.Play ();
		specialStripesTapped++;
		specialStripeMultiplier.GetComponent<UILabel>().text = "x" + specialStripesTapped.ToString ();
		Vector3 multiplierPosition = specialStripeMultiplier.transform.position;
		multiplierPosition.y = specialStripePos;
		specialStripeMultiplier.transform.position = multiplierPosition;
		specialStripeMultiplier.animation.Play();
	}

	public void OnGameEnd () {
		gameEnd = true;
		mainPaperPiece.rigidbody2D.isKinematic = false;
		paperCorner.SetActive (false);
		paperBody.SetActive (false);
	}
}
