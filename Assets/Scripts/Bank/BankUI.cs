﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public enum View {
	Hidden,
	LogIn,
	Home,
	Leaderboard
}

public class BankUI : MonoBehaviour {
	private int score;
	private View view = View.Hidden;

	[SerializeField]
	private MainMenu mainMenu;
	[SerializeField]
	private BankManager bankManager;
	[SerializeField]
	private UILabel scoreLabel;
	[SerializeField]
	private UILabel nameLabel;
	[SerializeField]
	private UILabel floatingRollsScoreLabel;
	[SerializeField]
	private UILabel unRollScoreLabel;
	[SerializeField]
	private UIButton backButton;
	[SerializeField]
	private UISprite homeImage;
	[SerializeField]
	private UIButton logInButton;
	[SerializeField]
	private UIButton leaderboardButton;
	[SerializeField]
	private UIButton logOutButton;
	[SerializeField]
	private UILabel loadingLabel;
	[SerializeField]
	private GameObject footprintsGroup;
	[SerializeField]
	private UIScrollView leaderboardList;
	[SerializeField]
	private GameObject playerScorePrefab;

	private List<PlayerScore> leaderboardItems = new List<PlayerScore>();
	
	public void OnBack () {
		if (this.view == View.LogIn || this.view == View.Home) {
			this.OnBankOut ();
		} else if (this.view == View.Leaderboard) {
			this.OnHome ();
		}
	}

	public void Show () {
		StartCoroutine (RunShow ());
	}

	IEnumerator RunShow () {
		bool infoReceived = false;

		if (bankManager.IsLoggedIn) {
			bankManager.OnLogIn (() => {
				infoReceived = true;
			});
		}

		logInButton.GetComponent<UISprite>().alpha = 0;
		leaderboardButton.GetComponent<UISprite>().alpha = 0;
		logOutButton.GetComponent<UISprite>().alpha = 0;
		footprintsGroup.GetComponentsInChildren<UIWidget>().ToList ().ForEach (w => w.alpha = 0);
		nameLabel.alpha = 0;

		backButton.enabled = false;
		logInButton.enabled = false;
		leaderboardButton.enabled = false;
		logOutButton.enabled = false;

		animation.Play("BankUIIn");
		yield return new WaitForSeconds (animation["BankUIIn"].length);
		backButton.enabled = true;

		loadingLabel.animation.Play ("FadeIn");
		yield return new WaitForSeconds (loadingLabel.animation["FadeIn"].length);
		view = View.LogIn;

		while (!FB.IsInitialized)
			yield return null;

		if (bankManager.IsLoggedIn) {
			while (!infoReceived)
				yield return null;
		}

		loadingLabel.animation.Play ("FadeOut");
		yield return new WaitForSeconds (loadingLabel.animation["FadeOut"].length);

		if (!bankManager.IsLoggedIn) {
			logInButton.animation.Play ("FadeIn");
			yield return new WaitForSeconds (logInButton.animation["FadeIn"].length);
			logInButton.enabled = true;
		} else {
			nameLabel.animation.Play ("FadeIn");
			footprintsGroup.animation.Play ("FadeIn");
			leaderboardButton.animation.Play ("FadeIn");
			logOutButton.animation.Play ("FadeIn");
			yield return new WaitForSeconds (footprintsGroup.animation["FadeIn"].length);
			leaderboardButton.enabled = true;
			logOutButton.enabled = true;
			view = View.Home;
		}
	}

	private void OnBankOut () {
		StartCoroutine (RunOnBankOut());
	}

	IEnumerator RunOnBankOut () {
		yield return StartCoroutine (Hide ());
		mainMenu.OnBankOut ();
	}

	IEnumerator Hide () {
		backButton.enabled = false;
		logInButton.enabled = false;
		leaderboardButton.enabled = false;
		logOutButton.enabled = false;
		view = View.Hidden;
		animation.Play("BankUIOut");
		yield return new WaitForSeconds (animation["BankUIOut"].length);
	}

	public void OnLogIn () {
		StartCoroutine (RunOnLogIn ());
	}

	IEnumerator RunOnLogIn () {
		bool isLogged = false;

		logInButton.enabled = false;
		logInButton.animation.Play ("FadeOut");

		bankManager.FBLogIn (() => {
			isLogged = true;
		});
		yield return new WaitForSeconds (logInButton.animation["FadeOut"].length);
		loadingLabel.animation.Play ("FadeIn");
		yield return new WaitForSeconds (loadingLabel.animation["FadeIn"].length);

		while (!isLogged)
			yield return null;

		loadingLabel.animation.Play ("FadeOut");
		yield return new WaitForSeconds (loadingLabel.animation["FadeOut"].length);

		nameLabel.animation.Play ("FadeIn");
		footprintsGroup.animation.Play ("FadeIn");
		leaderboardButton.animation.Play ("FadeIn");
		logOutButton.animation.Play ("FadeIn");
		yield return new WaitForSeconds (footprintsGroup.animation["FadeIn"].length);
		leaderboardButton.enabled = true;
		logOutButton.enabled = true;
		view = View.Home;
	}

	public void UpdateUserInfo (string name, int score, Texture2D photo) {
		nameLabel.text = name;
		scoreLabel.text = score.ToString ();
		unRollScoreLabel.text = PlayerPrefs.GetInt ("UnRollTopScore").ToString ();
		floatingRollsScoreLabel.text = PlayerPrefs.GetInt ("FloatingRollsTopScore").ToString ();
	}

	public void ClearUserInfo () {
		nameLabel.text = "";
		scoreLabel.text = "";
	}

	public void OnHome () {
		StartCoroutine (LeaderboardOut ());
	}

	public void OnLeaderboard () {
		StartCoroutine (LeaderboardIn ());
	}

	IEnumerator LeaderboardIn () {
		bool populated = false;

		backButton.enabled = false;
		leaderboardButton.enabled = false;
		logOutButton.enabled = false;
		bankManager.PopulateLeaderboar (() => {
			populated = true;
			if (LoadingWindow.isOpen)
				LoadingWindow.Close ();
		});
		animation.Play ("LeaderboardIn");
		yield return new WaitForSeconds(animation["LeaderboardIn"].length);

		backButton.enabled = true;

		if (!populated)
			LoadingWindow.Show ();
		
		view = View.Leaderboard;
	}

	IEnumerator LeaderboardOut () {
		backButton.enabled = false;
		leaderboardButton.enabled = false;
		logOutButton.enabled = false;
		animation.Play ("LeaderboardOut");
		yield return new WaitForSeconds(animation["LeaderboardOut"].length);
		backButton.enabled = true;
		leaderboardButton.enabled = true;
		logOutButton.enabled = true;
		view = View.Home;
	}

	public int GetScore () {
		return this.score;
	}

	public void SetScore (int newScore) {
		this.score = newScore;
		scoreLabel.text = newScore.ToString ();
	}

	public void ClearLeaderboard () {
		leaderboardItems.ForEach (i => {
			Destroy (i.gameObject);
		});

		leaderboardItems.Clear ();
	}

	public void AddLeaderboardItem (string rank, string name, string score) {
		GameObject playerScoreInstance = Instantiate (playerScorePrefab) as GameObject;
		playerScoreInstance.transform.parent = leaderboardList.transform;
		playerScoreInstance.transform.localScale = Vector3.one;
		playerScoreInstance.GetComponent<PlayerScore>().SetInfo (rank, name, score);
		Vector3 position = new Vector3(0, 205 - 55 * ((int.Parse (rank)) - 1), 0);
		playerScoreInstance.transform.localPosition = position;

		leaderboardItems.Add (playerScoreInstance.GetComponent<PlayerScore>());
	}

	public void UpdateLeaderboard () {
		leaderboardList.UpdatePosition ();
		leaderboardList.UpdateScrollbars (true);
		leaderboardList.verticalScrollBar.value = 0;
	}

	public void OnLogOut () {
		bankManager.OnLogOut ();
		this.OnBankOut ();
	}
}
