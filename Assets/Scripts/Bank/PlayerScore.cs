﻿using UnityEngine;
using System.Collections;

public class PlayerScore : MonoBehaviour {
	[SerializeField]
	private UILabel rank;
	[SerializeField]
	private UILabel name;
	[SerializeField]
	private UILabel score;
	[SerializeField]
	private UISprite background;

	public void SetInfo (string rank, string name, string score) {
		this.rank.text = rank;
		this.name.text = name;
		this.score.text = score;

		if ((int.Parse (rank) % 2) != 0)
			background.alpha = 0;
	}
}
