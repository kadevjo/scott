﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class BankManager : MonoBehaviour {
	[SerializeField]
	private BankUI bankUI;

	private bool isLoggedIn;
	public bool IsLoggedIn {
		get {return FB.IsLoggedIn;}
	}

	public string id;
	public string name;
	public Texture2D photo;
	public int score;

	public void ShowUI () {
		bankUI.Show ();
	}

	public void FBLogIn (System.Action callback) {
		FacebookManager.Instance.LogIn (() => {
			score = PlayerPrefs.GetInt ("score");
			FacebookManager.Instance.PostScore (score);

			OnLogIn (() => callback());
		});
	}

	public void OnLogIn (System.Action onFinish) {
		FacebookManager.Instance.GetProfileData (d => {
			id = (string)d["id"];
			name = (string)d["name"];

			FacebookManager.Instance.GetScore (s => {
				score = s;
				bankUI.UpdateUserInfo (name, s, photo);
				onFinish();
			});
		});		
	}

	public void PopulateLeaderboar (System.Action onFinish) {
		bankUI.ClearLeaderboard ();

		FacebookManager.Instance.GetLeaderBoard (players => {
			int rank = 0;

			players.OrderByDescending (p => p.score).ToList ().ForEach (p => {
				rank++;
				bankUI.AddLeaderboardItem (rank.ToString (), p.name, p.score.ToString ());
			});

			onFinish();
		});
	}

	public void OnLogOut () {
		FacebookManager.Instance.LogOut ();
	}
}
