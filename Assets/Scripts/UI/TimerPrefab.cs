﻿using UnityEngine;
using System.Collections;
using System;

public class TimerPrefab : MonoBehaviour {

	public Action onSecondChange;
	public float currentTime = 0;

	[SerializeField]
	private UILabel timeLabel;
	
	private int frameCount = 0;
	private bool stopTimer = true;
	private int lastSecond = 0;
	private int currentSecond = 0;
	
	// Use this for initialization
	private void Start () {
		timeLabel.text = "00:00";
		currentTime = 0;
	}

	public void Run(){
		//if (stopTimer == false)
		//	return;
		stopTimer = false;
		StartCoroutine ("runTimer");
	}

	public void Stop(){
		stopTimer = true;
		currentTime = 0;
	}

	public void Pause(){
		stopTimer = true;
	}

	public void Resume(){
		Run ();
	}
	
	IEnumerator runTimer(){
		while (!stopTimer) {
			if (!(frameCount < 10)) {
				frameCount = 0;
			}else{
				frameCount++;
				
				currentTime += Time.deltaTime;
				TimeSpan time = TimeSpan.FromSeconds( currentTime );
				
				//int miliseconds = (time.Milliseconds > 99) ? (int)(time.Milliseconds / 10) : time.Milliseconds;
				timeLabel.text = string.Format("{0:D2}:{1:D2}",
				                               time.Minutes, 
				                               time.Seconds/*, 
				                               miliseconds*/);

				currentSecond = time.Seconds;

				if ((currentSecond-lastSecond) != 0) {
					lastSecond = currentSecond;

					if (onSecondChange != null)
						onSecondChange();
				}
			}
			yield return null;
		}
	}
}
