﻿using UnityEngine;
using System.Collections;


public class MainWindow : Window<MainWindow> {
	

	public static void Show (){
		_Show ("MainWindow");
	}


	public void OnUnrollGamePress(){
		Debug.Log ("Pressed");
		SplashWindow.Show (SplashWindow.SplashType.desenrrolla_splash, ()=>{
			
			Application.LoadLevelAsync("UnRollGameScene");
			
		});
	}

	public void OnFloatGamePress(){
		SplashWindow.Show (SplashWindow.SplashType.rollos_flotantes_splash, ()=>{

			Application.LoadLevelAsync("FloatingRollsGameScene");

		});

	}

	
	protected override void Initialize ()
	{

	}

}
