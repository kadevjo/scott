﻿using UnityEngine;
using System.Collections;

public class MainMenuButton : MonoBehaviour {
	[SerializeField]
	private MainMenu mainMenu;

	public System.Action OnClick;
	public bool isClicked;
	public bool isInteractable;

	void OnMouseDown () {
		OnClickDown ();
	}

	public void OnClickDown () {
		if (isInteractable) {
			isClicked = true;
			
			if (OnClick != null)
				OnClick();
		}
	}
}
