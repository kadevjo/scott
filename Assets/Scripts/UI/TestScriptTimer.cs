﻿using UnityEngine;
using System.Collections;

public class TestScriptTimer : MonoBehaviour {


	public bool isActive = true;

	public TimerPrefab timerPrefab;

	// Update is called once per frame
	void Update () {

		if (isActive == true) {
			timerPrefab.Run ();
		} else {
			timerPrefab.Pause();
		}
		
	}
}
