﻿using UnityEngine;
using System.Collections;


public class SplashWindow : Window<SplashWindow> {


	System.Action onFinish;

	public enum SplashType{desenrrolla_splash, rollos_flotantes_splash};

	[SerializeField]
	private UISprite SplashLogo;

	[SerializeField]
	private float time = 3;


	public static void Show (SplashType type, System.Action onFinishCallback = null){
		_Show ("SplashWindow", ()=>{
			Instance.SplashLogo.spriteName = type.ToString();

			if(onFinishCallback != null){
				Instance.onFinish += onFinishCallback;
				Instance.onFinish += ()=>{Close(false);};
			}
			else{
				Instance.onFinish += ()=>{Close();};
			}

		});

	}	
	
	protected override void Initialize ()
	{
		StartCoroutine ("Wait");
	}

	IEnumerator Wait(){
		yield return new WaitForSeconds (time);
		onFinish ();
	}
	
}
