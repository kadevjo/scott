﻿using UnityEngine;
using System.Collections;

public class EndOfGameWindow : Window<EndOfGameWindow> {

	public static string endingGameScene = "";

	[SerializeField]
	private UILabel currentScore;
	[SerializeField]
	private UILabel bestScore;
	[SerializeField]
	private UILabel footprints;

	[SerializeField]
	private GameObject megasuavePack;
	[SerializeField]
	private GameObject rindemaxPack;

	[SerializeField]
	private GameObject newRecordLabel;

	private int score;

	public void ShowPack (string gameSceneName) {
		if (gameSceneName == "UnRollGameScene") {
			megasuavePack.SetActive (false);
		} else {
			rindemaxPack.SetActive (false);
		}
	}

	public static void Show (string gameSceneName, int currentScore, int bestScore, bool isNewRecord){
		endingGameScene = gameSceneName;	

		_Show ("EndOfGameWindow", () => {
			Instance.score = currentScore;
			Instance.currentScore.text = currentScore.ToString();
			Instance.bestScore.text = bestScore.ToString();
			Instance.newRecordLabel.SetActive (isNewRecord);

			int footprintsScore = PlayerPrefs.GetInt ("score");

			if (isNewRecord) {
				footprintsScore += 6;
				Instance.footprints.text = "x6";
			} else {
				footprintsScore += 2;
				Instance.footprints.text = "x2";
			}

			PlayerPrefs.SetInt ("score", footprintsScore);

			if (FB.IsLoggedIn)
				FacebookManager.Instance.PostScore (footprintsScore);
		});		
	}

	public void RestartGame(){
		if (endingGameScene != "") Application.LoadLevelAsync(endingGameScene);
		Close (false);
	}

	public void GoHome(){
		Application.LoadLevelAsync ("MainMenu");
		Close (false);
	}
}
