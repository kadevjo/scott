﻿using UnityEngine;
using System;
using System.Collections;

using Scott.FloatingPaper;

public class FloatingRollsUI : MonoBehaviour {
	[SerializeField]
	private UIButton homeButton;
	[SerializeField]
	private UIButton pauseButton;
	[SerializeField]
	private UIButton helpButton;
	[SerializeField]
	private UIButton closeHelpButton;
	[SerializeField]
	private Animation pauseAnimation;
	[SerializeField]
	private Animation tutorialAnimation;
	[SerializeField]
	private UILabel tutorialCounter;
	[SerializeField]
	private UILabel scoreLabel;
	[SerializeField]
	private UILabel recordLabel;

	[SerializeField]
	private FloatingRollsGameLogic gameManager;

	private bool isPaused;
	private bool isTutorialRunning;

	void Start () {
		homeButton.isEnabled = false;
		closeHelpButton.isEnabled = false;

		helpButton.isEnabled = true;
		this.recordLabel.text = PlayerPrefs.GetInt ("FloatingRollsTopScore").ToString();
	}

	public void OnPauseClick () {
		if (isPaused) {
			StartCoroutine (OnResume ());
		} else {
			StartCoroutine (OnPause ());
		}
	}

	IEnumerator OnPause () {
		this.isPaused = true;
		pauseButton.isEnabled = false;
		helpButton.isEnabled = false;
		gameManager.PauseGame ();
		pauseAnimation.Play ("OnPauseGame");
		yield return new WaitForSeconds (pauseAnimation["OnPauseGame"].length);
		pauseButton.isEnabled = true;
		homeButton.isEnabled = true;
		helpButton.isEnabled = true;
	}

	IEnumerator OnResume () {
		this.isPaused = false;
		pauseButton.isEnabled = false;
		homeButton.isEnabled = false;
		helpButton.isEnabled = false;
		pauseAnimation.Play ("OnResumeGame");
		yield return new WaitForSeconds (pauseAnimation["OnResumeGame"].length);
		gameManager.ResumeGame ();
		pauseButton.isEnabled = true;
		helpButton.isEnabled = true;
	}

	public void OnHome () {
		Physics2D.gravity = gameManager.gravity;
		Application.LoadLevelAsync ("MainMenu");
	}

	public void OnHelp () {
		StartCoroutine ("RunTutorial");
	}

	IEnumerator RunTutorial () {
		if (!this.isPaused)
			gameManager.PauseGame ();

		pauseButton.isEnabled = false;
		homeButton.isEnabled = false;
		helpButton.isEnabled = false;
		tutorialAnimation.Play ("FadeIn");
		yield return new WaitForSeconds (tutorialAnimation["FadeIn"].length);
		this.isTutorialRunning = true;
		closeHelpButton.isEnabled = true;
		tutorialAnimation.Play ("tutorial_floatingrolls");
		yield return StartCoroutine (TutorialTime (tutorialAnimation["tutorial_floatingrolls"].length));
		this.isTutorialRunning = false;
		yield return StartCoroutine (CloseTutorial ());
	}

	IEnumerator TutorialTime (float tutorialTime) {
		float currentTime = 0;

		while (currentTime < tutorialTime) {
			currentTime += Time.deltaTime;
			TimeSpan time = TimeSpan.FromSeconds(currentTime);
			tutorialCounter.text = string.Format("{0:D2}:{1:D2}", time.Minutes, time.Seconds);
			yield return null;
		}
	}

	public void OnHelpClose () {
		StartCoroutine (CloseTutorial ());
	}

	IEnumerator CloseTutorial () {
		if (this.isTutorialRunning) {
			StopCoroutine ("RunTutorial");
			tutorialAnimation["tutorial_floatingrolls"].speed = 0;
		}

		closeHelpButton.isEnabled = false;
		tutorialAnimation.Play ("FadeOut");
		yield return new WaitForSeconds (tutorialAnimation["FadeOut"].length);
		pauseButton.isEnabled = true;
		helpButton.isEnabled = true;

		if (!this.isPaused) {
			gameManager.ResumeGame ();	
		} else {
			homeButton.isEnabled = true;
		}

		tutorialAnimation["tutorial_floatingrolls"].speed = 1;
		tutorialAnimation["tutorial_floatingrolls"].time = 0;
		tutorialAnimation.Play ("tutorial_floatingrolls");
		yield return new WaitForSeconds (0.02f);
		tutorialAnimation.Stop ("tutorial_floatingrolls");
	}

	public void SetScore (int score) {
		this.scoreLabel.text = score.ToString ();
	}
}