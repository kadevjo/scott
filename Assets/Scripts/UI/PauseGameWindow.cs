﻿using UnityEngine;
using System.Collections;

public class PauseGameWindow : Window<PauseGameWindow> {

	// Use this for initialization
	void Start () {
	
	}


	public static void Show (System.Action onFinishCallback = null){
		
		_Show ("PauseGameWindow");
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
