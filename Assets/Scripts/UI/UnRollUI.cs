﻿using UnityEngine;
using System;
using System.Collections;

public class UnRollUI : MonoBehaviour {
	[SerializeField]
	private UIButton homeButton;
	[SerializeField]
	private UIButton pauseButton;
	[SerializeField]
	private UIButton helpButton;
	[SerializeField]
	private UIButton closeHelpButton;
	[SerializeField]
	private Animation tutorialAnimation;
	[SerializeField]
	private Animation pauseAnimation;
	[SerializeField]
	private UILabel tutorialCounter;
	[SerializeField]
	private UILabel scoreLabel;
	[SerializeField]
	private UILabel recordLabel;

	[SerializeField]
	private UnRollGameLogic gameManager;
	
	private bool isPaused;
	private bool isTutorialRunning;
	
	void Start () {
		homeButton.isEnabled = false;
		this.recordLabel.text = PlayerPrefs.GetInt ("UnRollTopScore").ToString();
	}
	
	public void OnPauseClick () {
		if (isPaused) {
			StartCoroutine (OnResume ());
		} else {
			StartCoroutine (OnPause ());
		}
	}
	
	IEnumerator OnPause () {
		this.isPaused = true;
		pauseButton.isEnabled = false;
		gameManager.PauseGame ();
		pauseAnimation.Play ("OnPauseGameUnRoll");
		yield return new WaitForSeconds (pauseAnimation["OnPauseGameUnRoll"].length);
		pauseButton.isEnabled = true;
		homeButton.isEnabled = true;
	}
	
	IEnumerator OnResume () {
		this.isPaused = false;
		pauseButton.isEnabled = false;
		homeButton.isEnabled = false;
		pauseAnimation.Play ("OnResumeGameUnRoll");
		yield return new WaitForSeconds (pauseAnimation["OnResumeGameUnRoll"].length);
		gameManager.ResumeGame ();
		pauseButton.isEnabled = true;
	}
	
	public void OnHome () {
		Application.LoadLevelAsync ("MainMenu");
	}

	public void OnHelp () {
		if (!this.isPaused) {
			gameManager.PauseGame ();
		}

		StartCoroutine ("RunTutorial");
	}
	
	IEnumerator RunTutorial () {
		pauseButton.isEnabled = false;
		homeButton.isEnabled = false;
		helpButton.isEnabled = false;
		tutorialAnimation.Play ("FadeIn");
		yield return new WaitForSeconds (tutorialAnimation["FadeIn"].length);
		this.isTutorialRunning = true;
		closeHelpButton.isEnabled = true;
		tutorialAnimation.Play ("tutorial_unroll");
		yield return StartCoroutine (TutorialTime (tutorialAnimation["tutorial_unroll"].length));
		this.isTutorialRunning = false;
		yield return StartCoroutine (CloseTutorial ());
	}
	
	IEnumerator TutorialTime (float tutorialTime) {
		float currentTime = 0;
		
		while (currentTime < tutorialTime) {
			currentTime += Time.deltaTime;
			TimeSpan time = TimeSpan.FromSeconds(currentTime);
			tutorialCounter.text = string.Format("{0:D2}:{1:D2}", time.Minutes, time.Seconds);
			yield return null;
		}
	}

	public void OnHelpClose () {
		StartCoroutine (CloseTutorial ());
	}
	
	IEnumerator CloseTutorial () {
		if (this.isTutorialRunning) {
			StopCoroutine ("RunTutorial");
			tutorialAnimation["tutorial_unroll"].speed = 0;
		}
		
		closeHelpButton.isEnabled = false;
		tutorialAnimation.Play ("FadeOut");
		yield return new WaitForSeconds (tutorialAnimation["FadeOut"].length);
		pauseButton.isEnabled = true;
		homeButton.isEnabled = true;
		helpButton.isEnabled = true;

		if (!this.isPaused) {
			gameManager.ResumeGame ();
		}

		tutorialAnimation["tutorial_unroll"].speed = 1;
		tutorialAnimation["tutorial_unroll"].time = 0;
		tutorialAnimation.Play ("tutorial_unroll");
		yield return new WaitForSeconds (0.02f);
		tutorialAnimation.Stop ("tutorial_unroll");
	}

	public void SetScore (int score) {
		this.scoreLabel.text = score.ToString ();
	}
}
