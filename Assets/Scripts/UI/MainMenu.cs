﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {
	[SerializeField]
	private Animation menuAnimation;
	[SerializeField]
	private Animation menuAnimationScenario;
	[SerializeField]
	private MainMenuButton unrollButton;
	[SerializeField]
	private MainMenuButton floatingRollsButton;
	[SerializeField]
	private MainMenuButton bankButton;
	[SerializeField]
	private UIButton closeButton;
	[SerializeField]
	private UITexture floatingRollsBackground;
	[SerializeField]
	private UITexture unrollBackground;
	[SerializeField]
	private UITexture bankBackground;
	[SerializeField]
	private BankManager bankManager;

	private bool isInMainMenu;

	void Awake () {
		Screen.orientation = ScreenOrientation.Portrait;
		unrollButton.OnClick += OnUnrollButtonClick;
		floatingRollsButton.OnClick += OnFloatingRollsButtonClick;
		bankButton.OnClick += OnBankButtonClick;
	}

	void Start () {
		if (!FB.IsInitialized) {
			FB.Init (() => {
				if (bankManager.IsLoggedIn) {
					FacebookManager.Instance.GetScore (s => {
						PlayerPrefs.SetInt ("score", s);
						PlayerPrefs.Save ();
					});
				}
			});
		}

		this.isInMainMenu = true;
	}

	void Update () {
#if UNITY_ANDROID
		if (Input.GetKeyUp (KeyCode.Escape) && this.isInMainMenu) {
			this.OnClose ();
		}
#endif
	}

	private void OnUnrollButtonClick () {
		this.isInMainMenu = false;
		this.DisableButtons ();
		StartCoroutine (UnrollClick ());
	}

	IEnumerator UnrollClick () {
		menuAnimation.Play ("RindeRolloTransition");
		yield return new WaitForSeconds (menuAnimation["RindeRolloTransition"].length + 0.5f);
		menuAnimationScenario.Play ("RinderolloOut");
		yield return new WaitForSeconds (menuAnimationScenario["RinderolloOut"].length);
		yield return StartCoroutine (FadeInGameBackground (unrollBackground, 1));
		Application.LoadLevelAsync ("UnrollGameScene");
	}

	private void OnFloatingRollsButtonClick () {
		this.isInMainMenu = false;
		this.DisableButtons ();
		StartCoroutine (FloatingRollsClick ());
	}

	IEnumerator FloatingRollsClick () {
		menuAnimation.Play ("MegaSuaveTransition");
		yield return new WaitForSeconds (menuAnimation["MegaSuaveTransition"].length + 0.25f);
		menuAnimationScenario.Play ("MegasuaveOut");
		yield return new WaitForSeconds (menuAnimationScenario["MegasuaveOut"].length);
		yield return StartCoroutine (FadeInGameBackground (floatingRollsBackground, 1));
		Application.LoadLevelAsync ("FloatingRollsGameScene");
	}

	private void OnBankButtonClick () {
		this.isInMainMenu = false;
		this.DisableButtons ();
		StartCoroutine (BankClick ());
	}

	IEnumerator BankClick () {
		menuAnimationScenario.Play ("Bank_Camera_In");
		yield return new WaitForSeconds (menuAnimationScenario["Bank_Camera_In"].length - 0.5f);
		bankManager.ShowUI ();
	}

	IEnumerator FadeInGameBackground (UITexture background, float time) {
		float currentTime = 0;
		
		while (currentTime < time) {
			currentTime += Time.deltaTime;
			Color imageColor = background.GetComponent<UITexture>().color;
			imageColor.a = Mathf.Lerp (0, 1, currentTime / time);
			background.GetComponent<UITexture>().color = imageColor;
			yield return null;
		}
	}

	private void EnableButtons () {
		unrollButton.isInteractable = true;
		floatingRollsButton.isInteractable = true;
		bankButton.isInteractable = true;
		bankButton.GetComponent<UIButton>().enabled = true;
	}

	private void DisableButtons () {
		bankButton.animation.Play ("FadeOut");
		closeButton.animation.Play ("FadeOut");
		unrollButton.isInteractable = false;
		floatingRollsButton.isInteractable = false;
		bankButton.isInteractable = false;
		bankButton.GetComponent<UIButton>().enabled = false;
	}

	public void OnBankOut () {
		StartCoroutine (RunOnBankOut());
	}

	IEnumerator RunOnBankOut () {
		menuAnimationScenario.Play ("Bank_Camera_Out");
		float bankButtonFadeTime = menuAnimationScenario["Bank_Camera_Out"].length - bankButton.animation["FadeIn"].length;
		yield return new WaitForSeconds (bankButtonFadeTime);
		bankButton.animation.Play ("FadeIn");
		closeButton.animation.Play ("FadeIn");
		yield return new WaitForSeconds (bankButton.animation["FadeIn"].length);
		this.EnableButtons ();
		this.isInMainMenu = true;
	}

	void OnDestroy () {
		unrollButton.OnClick -= OnUnrollButtonClick;
		floatingRollsButton.OnClick -= OnFloatingRollsButtonClick;
		bankButton.OnClick -= OnBankButtonClick;
	}

	private void OnClose () {
		this.DisableButtons ();
		Application.Quit ();
	}
}
