﻿using UnityEngine;
using System.Collections;

public class PaperRollGenerator : MonoBehaviour {


	public int rollsCount = 0;

	[SerializeField]
	public int maxRolls = 10;

	[SerializeField]
	private float lowerInterval = 3.5f;
	[SerializeField]
	private float upperInterval = 6.5f;
	[SerializeField]
	private GameObject leftAnchor;
	[SerializeField]
	private GameObject rightAnchor;
	[SerializeField]
	private GameObject paperRollsParent;
	[SerializeField]
	private Sprite normalPaperRollSprite;
	[SerializeField]
	private Sprite specialPaperRollSprite;
	[SerializeField]
	private AudioSource sfx;

	private float screenWidth;
	private bool generate = true;

	void Start () {
		screenWidth = leftAnchor.transform.localPosition.x - rightAnchor.transform.localPosition.x;
	}

	void AddPaperRoll () {
		string paperRollType = "NormalPaperRoll";
		GameObject paperRoll = Instantiate (Resources.Load ("PaperRolls/" + paperRollType)) as GameObject;

		bool isSpecialPaperRoll = Random.Range (0f, 1f) > 0.75f ? true : false;
		Sprite paperRollSprite = isSpecialPaperRoll ? specialPaperRollSprite : normalPaperRollSprite;

		paperRoll.GetComponent<SpriteRenderer>().sprite = paperRollSprite;
		paperRoll.SetLayerRecursively (LayerMask.NameToLayer("TouchManager"));
		paperRoll.transform.parent = paperRollsParent.transform;
		float posX = Random.Range (-0.45f, 0.45f) * screenWidth;
		float topBorder = Camera.main.ScreenToWorldPoint(new Vector3(0, Screen.height, 0)).y;
		topBorder = topBorder / Camera.main.transform.parent.localScale.y;
		paperRoll.transform.localPosition = new Vector3 (posX , topBorder * 1.3f, 0);
		paperRoll.rigidbody2D.AddTorque (UnityEngine.Random.Range (-400, 400));
		paperRoll.GetComponent<PaperRoll>().sfx = this.sfx;
		paperRoll.GetComponent<PaperRoll>().isSpecial = isSpecialPaperRoll;
		rollsCount++;
	}

	public void StartGenerating(){
		generate = true;
		StartCoroutine ("PaperGenerator");
	}

	public void StopGenerating(){
		generate = false;
	}

	IEnumerator PaperGenerator(){
		while (generate) {
			if(rollsCount < maxRolls)
				AddPaperRoll ();

			yield return new WaitForSeconds (Random.Range (lowerInterval, upperInterval));
		}
	}

	public PaperRoll[] GetPaperRolls () {
		return paperRollsParent.GetComponentsInChildren<PaperRoll>();
	}
}
