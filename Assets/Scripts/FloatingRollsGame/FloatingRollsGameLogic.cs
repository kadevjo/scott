﻿using UnityEngine;
using System.Linq;
using System.Collections;

namespace Scott.FloatingPaper{

	[RequireComponent(typeof(TouchManager))]
	[RequireComponent(typeof(PaperRollGenerator))]
	[RequireComponent(typeof(BottomCollisionTrigger))]

	public class FloatingRollsGameLogic : MonoBehaviour {

		/// <summary>
		/// Number of tries before ending the game
		/// </summary>
		public int tries = 1;
		public int currentScore = 0;
		public bool isPaused;
		public Vector3 gravity;

		private bool increaseDifficulty = true;

		[SerializeField]
		private bool debug = true;
		[SerializeField]
		private TimerPrefab timer;
		[SerializeField]
		private GameObject[] triesLost = new GameObject[3];
		[SerializeField]
		private BottomCollisionTrigger bottomCollisionObject;
		[SerializeField]
		private UITexture difficultyWall;
		[SerializeField]
		private AudioSource music;
		[SerializeField]
		private AudioSource sfx;
		[SerializeField]
		private AudioClip paperFallClip;
		[SerializeField]
		private FloatingRollsUI floatingRollsUI;
		[SerializeField]
		private int normalPaperRollScore = 50;
		[SerializeField]
		private int specialPaperRollScore = 100;
		[SerializeField]
		private GameObject multipliersContainer;

		private TouchManager touchManager;
		private PaperRollGenerator paperRollGenerator;
		private float wallPos = 0;
		private float maxGrowth = 0;

		[SerializeField]
		private float screenHeight = 0;
		
		void Start () {
			float bottomBorder = Camera.main.ScreenToWorldPoint(Vector3.zero).y;
			float topBorder = Camera.main.ScreenToWorldPoint(new Vector3(0, Screen.height, 0)).y;
			screenHeight =  topBorder - bottomBorder;
			screenHeight = screenHeight / Camera.main.transform.parent.localScale.y;
			maxGrowth = screenHeight * 0.55f * 2; // Multiplied by 2 because of the wall is rendered half of it

			touchManager = GetComponent<TouchManager> ();
			paperRollGenerator = GetComponent<PaperRollGenerator> ();
			bottomCollisionObject.onCollision += OnBottomCollision;

			touchManager.onPaperRollTouch = OnPaperRollTouch;

			startGame ();
		}

		public void startGame(){
			timer.Run();
			touchManager.enabled = true;
			paperRollGenerator.StartGenerating();
		}

		public void PauseGame(){
			isPaused = true;
			timer.Pause ();
			touchManager.touchEnabled = false;
			paperRollGenerator.StopGenerating ();
			paperRollGenerator.GetPaperRolls ().ToList ().ForEach (p => p.OnPause ());
			this.gravity = Physics2D.gravity;
			Physics2D.gravity = Vector3.zero;
			music.volume = 0.5f;
		}

		public void ResumeGame(){
			isPaused = false;
			timer.Resume ();
			touchManager.touchEnabled = true;
			paperRollGenerator.StartGenerating();
			paperRollGenerator.GetPaperRolls ().ToList ().ForEach (p => p.OnResume ());
			Physics2D.gravity = gravity;
			music.volume = 1;
		}

		public void EndGame(){
			isPaused = true;
			timer.Stop ();
			touchManager.enabled = false;
			paperRollGenerator.StopGenerating ();

			int bestScore = PlayerPrefs.GetInt ("FloatingRollsTopScore");
			bool isNewRecord = false;

			if(currentScore > bestScore){
				isNewRecord = true;
				bestScore = currentScore;
				PlayerPrefs.SetInt("FloatingRollsTopScore", currentScore);
				PlayerPrefs.Save ();
			}

			EndOfGameWindow.Show ("FloatingRollsGameScene", currentScore, bestScore, isNewRecord);
			EndOfGameWindow.Instance.ShowPack ("FloatingRollsGameScene");
		}

		private void OnBottomCollision(){
			tries--;
			paperRollGenerator.rollsCount--;

			sfx.clip = paperFallClip;
			sfx.Play ();

#if UNITY_ANDROID || UNITY_IPHONE
			//Handheld.Vibrate();
#endif

			if (tries > 0) {
				triesLost[tries].SetActive (true);		
			} else if (tries == 0) {
				triesLost[tries].SetActive (true);		
				EndGame();
			}
		}

		private void OnPaperRollTouch(bool isSpecial, Vector3 paperRollPosition){
			if (isSpecial) {
				currentScore += specialPaperRollScore;

				GameObject availableMultiplier = multipliersContainer.GetComponentsInChildren<Animation>().ToList ()
					.Where (m => !m.isPlaying).First ().gameObject;
				availableMultiplier.transform.position = paperRollPosition;
				availableMultiplier.animation.Play ();
			} else {
				currentScore += normalPaperRollScore;
			}

			floatingRollsUI.SetScore (currentScore);
		}
		
		void Update () {
			if (!isPaused)
				wallPos += Time.deltaTime;

			if ((int)wallPos % 10 == 0 && difficultyWall.height < maxGrowth && !isPaused) {
				difficultyWall.height += (int)(wallPos);

				GameObject go = difficultyWall.gameObject;

				BoxCollider2D collider = go.GetComponent<BoxCollider2D> ();
				UIWidget w = go.GetComponent<UIWidget> ();

				if (w != null) {
						Vector4 region = w.drawingDimensions;
						collider.center = new Vector3 ((region.x + region.z) * 0.5f, (region.y + region.w) * 0.5f);
						collider.size = new Vector3 (region.z - region.x, region.w - region.y);
				}
			}
		}
	}
}
