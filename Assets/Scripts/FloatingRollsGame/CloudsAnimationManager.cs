﻿using UnityEngine;
using System.Collections;

public class CloudsAnimationManager : MonoBehaviour {
	[SerializeField]
	private Animation cloudAnimation;
	[SerializeField]
	private float lowerVelocity;
	[SerializeField]
	private float upperVelocity;
	[SerializeField]
	private float lowerDelay;
	[SerializeField]
	private float upperDelay;

	private bool isAnimating;

	void Start () {
		Play ();
	}

	public void Play () {
		isAnimating = true;
		StartCoroutine ("CloudAnimation");
	}

	IEnumerator CloudAnimation () {
		while (isAnimating) {
			float animationSpeed = Random.Range (lowerVelocity, upperVelocity);
			cloudAnimation[cloudAnimation.clip.name].speed = animationSpeed;
			cloudAnimation.Play ();
			yield return new WaitForSeconds (cloudAnimation[cloudAnimation.clip.name].length / animationSpeed);
			yield return new WaitForSeconds (Random.Range (lowerDelay, upperDelay));
		}
	}
}
