﻿using UnityEngine;
using System.Collections;

namespace Scott.FloatingPaper{
	public class BottomCollisionTrigger : MonoBehaviour {

		public bool debug = true;

		public bool deleteObjects = true;

		/// <summary>
		/// Notifies to the suscriber when a collision is detected
		/// </summary>
		public System.Action onCollision;

		void OnTriggerEnter2D(Collider2D collider) {
			Destroy (collider.gameObject);
			onCollision ();
		}

	}
}

