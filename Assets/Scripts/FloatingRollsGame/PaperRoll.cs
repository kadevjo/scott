﻿using UnityEngine;
using System.Collections;

public class PaperRoll : MonoBehaviour {
	public AudioSource sfx;
	public bool isSpecial;

	[SerializeField]
	private AudioClip paperTapClip;

	private Vector3 velocity;
	private float angularVelocity;

	public void OnClick () {
		rigidbody2D.velocity = new Vector2(0, 10f);
		rigidbody2D.AddForce(new Vector2(Random.Range(-25.0f, 25.0f), 30f));
		rigidbody2D.AddTorque (UnityEngine.Random.Range (-200, 200));
		animation.Play ();
		sfx.clip = paperTapClip;
		sfx.Play ();
	}

	public void OnPause () {
		velocity = rigidbody2D.velocity;
		rigidbody2D.velocity = Vector3.zero;
		angularVelocity = rigidbody2D.angularVelocity;
		rigidbody2D.angularVelocity = 0;
	}

	public void OnResume () {
		rigidbody2D.velocity = velocity;
		rigidbody2D.angularVelocity = angularVelocity;
	}
}
