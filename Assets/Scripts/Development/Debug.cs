﻿
using System.Collections;




namespace Utils{


	public class Debug {

		private static bool production = false; 

		/// <summary>
		/// Log the specified author, message and print.
		/// </summary>
		/// <param name="author">Author.</param>
		/// <param name="message">Message.</param>
		/// <param name="print">If set to <c>true</c> print.</param>
		public static void Log (string author, string message, bool print){

			if (!production && print){

				UnityEngine.Debug.Log(string.Format("[{0}]: {1}", author, message));

			}


		}
	}


}

