﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class ObjectFade : MonoBehaviour {
	public float alpha;
	public bool isFading;
	private UIWidget widget;
	private UIWidget[] widgets;

	[SerializeField]
	private bool ignoreChildren;

	void Start () {
		this.widget = GetComponent<UIWidget>();
		this.widgets = GetComponentsInChildren<UIWidget>();
	}
	
	void Update () {		
		if (isFading) {
			Color newColor;

			if (this.widget != null) {
				newColor = this.widget.color;
				newColor.a = alpha;
				this.widget.color = newColor;
			}

			if (widgets.Length > 0 && !ignoreChildren) {
				widgets.ToList ().ForEach (w => {
					newColor = w.color;
					newColor.a = alpha;
					w.color = newColor;
				});
			}
		}
	}
}
