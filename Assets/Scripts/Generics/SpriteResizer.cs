﻿using UnityEngine;
using System.Collections;

public class SpriteResizer : MonoBehaviour {

	public float height;
	public float width;
	public bool isResizing;

	[SerializeField]
	private UISprite sprite;

	void Start () {
		height = sprite.height;
		width = sprite.width;
	}

	void Update () {
		if (isResizing) {
			sprite.SetDimensions ((int)width, (int)height);
		}
	}
}
