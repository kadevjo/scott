﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Singleton for non Monobehaviour classes
/// </summary>
public class Singleton<T>  where T : class, new(){
	
	private static T instance;
	
	public static T Instance{
		get{
			if(instance == null) instance = new T();
			return instance;
		}
	}
}
/// <summary>
/// Singleton for MonoBehaviour classes
/// </summary>
public class SingletonM<T> : MonoBehaviour where T : MonoBehaviour {
	
	private static T instance;
	
	public static T Instance{
		get{
			if(instance == null) CreateInstance();
			return instance;
		}
	}
	[SerializeField]
	private bool InScene = false;
	[SerializeField]
	private bool DestroyOnLoad = false;
	
	private static void CreateInstance(){
		var singleton = FindObjectOfType<T>();
		if(singleton == null){
			var go   = new GameObject();
			instance = go.AddComponent<T>();
			go.name = typeof(T).Name;
			DontDestroyOnLoad(go);
		}else{
			instance = singleton.GetComponent<T>();
		}
		
	}
	
	void Awake(){
		if(InScene){
			instance = gameObject.GetComponent<T>();;
			if(!DestroyOnLoad) DontDestroyOnLoad(this.gameObject);
		}
		OnAwake();
	}
	
	public virtual void OnAwake(){
		
	}
	//	void Awake(){
	//		//If instance doesn't exist
	//		if(instance != null){
	//			instance = GetComponent<T>();
	//			DontDestroyOnLoad(this.gameObject);
	//		}else{
	//			Destroy(this.gameObject);
	//		}
	//	}
	//	
}
