using UnityEngine;
using System.Collections;
using System.Linq;

public class TouchManager : MonoBehaviour {

	public bool debug = true; 	
	public bool touchEnabled = true;
	public LayerMask layer; 
	public System.Action<bool, Vector3> onPaperRollTouch;

	void Update () {
		if(!touchEnabled){
			return;
		}

#if UNITY_EDITOR
		//Clicks events handled in this area
		if(Input.GetMouseButtonDown(0)){ //Check if left button is pressed
			HandleClick(Input.mousePosition);
		}
#endif

#if UNITY_ANDROID || UNITY_IPHONE
		//Touch events handled in this area

		//Check if the user is touching the screen
		var tapCount = Input.touchCount;
		if (tapCount > 0){
			for ( var i = 0 ; i < tapCount ; i++ ) {
				var userTouch = Input.GetTouch(i);
				if (userTouch.phase == TouchPhase.Began)
					HandleClick(userTouch.position);
			}
		}
#endif
	
	}

	/// <summary>
	/// Handles clicks or touches for PaperRolls.
	/// </summary>
	/// <param name="position">Position.</param>
	void HandleClick(Vector3 position){
		Vector3 worldPoint = Camera.main.ScreenToWorldPoint(position);
		Vector2 clickPosition = new Vector2(worldPoint.x, worldPoint.y);
		
		RaycastHit2D hits = Physics2D.Raycast (clickPosition, Vector2.zero);
		
		if(hits != null && hits.collider != null){
			RaycastHit2D hit = hits;

			//Reference: http://answers.unity3d.com/questions/619090/touch-detection-in-2d-game.html
			if (hit.transform.tag == "PaperRoll") {
				hit.transform.GetComponent<PaperRoll>().OnClick();
				onPaperRollTouch(hit.transform.GetComponent<PaperRoll>().isSpecial, hit.transform.position);
			}
		}
	}

}
