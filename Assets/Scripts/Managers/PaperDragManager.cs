﻿using UnityEngine;
using System.Collections;

public class PaperDragManager : MonoBehaviour {

	public bool debug = true; 
	public bool isPaused;

	[SerializeField]
	private GameObject scrollableObject;
	
	private Vector3 worldPoint;
	private Vector3 initialTouch;
	private float deltaForceY;
	private float movingDeltaTime;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if (isPaused)
			return;

		worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);

#if UNITY_EDITOR
		worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		
		if(Input.GetMouseButtonDown(0)){ //Check if left button is pressed
			touchStart(worldPoint);
		}
		else if(Input.GetMouseButton(0)){
			touchMove(worldPoint);
		}
		
		if(Input.GetMouseButtonUp(0)){
			touchEnd();
		}
		
#endif
		
#if UNITY_ANDROID || UNITY_IPHONE
		var tapCount = Input.touchCount;
		if (tapCount > 0){
			for ( var i = 0 ; i < tapCount ; i++ ) {
				var userTouch = Input.GetTouch(i);
				worldPoint = Camera.main.ScreenToWorldPoint(userTouch.position);
				
				if (userTouch.phase == TouchPhase.Began){
					touchStart(worldPoint);
				}
				
				
				if (userTouch.phase == TouchPhase.Moved){
					touchMove(worldPoint);
					
				}
				
				if (userTouch.phase == TouchPhase.Ended){
					touchEnd();
				}
				
			}
		}
#endif
		
	}

//	void OnMouseDown () {
//		touchStart(worldPoint);
//		Debug.Log ("OnMouseDown");
//	}
//
//	void OnMouseDrag () {
//		touchMove(worldPoint);
//		Debug.Log ("OnMouseDrag");
//	}
//
//	void OnMouseUp () {
//		touchEnd();
//		Debug.Log ("OnMouseUp");
//	}

	void touchStart(Vector3 point){
		initialTouch = point;
		Utils.Debug.Log ("DragManager", "Initial touch at: "+point, debug);
	}
	
	void touchMove(Vector3 point){
		Vector3 delta = initialTouch - point;
		deltaForceY += delta.y;
		movingDeltaTime += Time.deltaTime;
		
		
		HandleDrag(delta);
		Utils.Debug.Log ("DragManager", "Moving from "+initialTouch+" to "+point+" (Delta: "+delta+")", debug);
		initialTouch = point;
		
	}
	
	void touchEnd(){
		if(deltaForceY > 0.05f)
			ApplyVelocity(deltaForceY);
		
		deltaForceY = 0;
		movingDeltaTime = 0;
	}
	
	void HandleDrag(Vector3 delta){
		Vector2 clickPosition = new Vector2(scrollableObject.transform.position.x,scrollableObject.transform.position.y-delta.y);

		if (clickPosition.y < scrollableObject.transform.position.y) {
			scrollableObject.transform.position = clickPosition;
		}
	}
	
	
	void ApplyVelocity (float deltaY){
		float velocity = (deltaY/movingDeltaTime);
		scrollableObject.rigidbody.velocity = new Vector3 (scrollableObject.rigidbody.velocity.x, -velocity, 0f);
	}
}
