﻿using UnityEngine;
using System.Collections;

public class DragManager : MonoBehaviour {

	public bool debug = true; 	
	public bool touchEnabled = true;

	[SerializeField]
	private GameObject scrollableObject;
	[SerializeField]
	private GameObject paperRoll; 
	[SerializeField]
	private UIProgressBar speedBar;
	[SerializeField]
	private TimerPrefab timer;	
	[SerializeField]
	private UISprite circle;
	[SerializeField]
	private UISprite paperMask;

	private GameObject paperRollPanel;
	private Vector3 worldPoint;
	private Vector3 initialTouch;
	private float deltaForceY;
	private float movingDeltaTime;
	private int distance = 0;

	int width;
	int height;
	float paperMaskY;

	// Use this for initialization
	void Start () {
		this.width = circle.width;
		this.height = circle.height;
		this.paperMaskY = paperMask.transform.position.y;
		this.paperRollPanel = paperRoll.transform.GetChild (0).gameObject;
		this.timer.Run ();
	}
	
	// Update is called once per frame
	void Update () {

		paperRollPanel.renderer.material.mainTextureOffset = new Vector2 (0, scrollableObject.transform.position.y);

		//Difficulty can be changed here
		distance = (int)(-scrollableObject.transform.position.y);//* 0.20f); // <- Debug
		float heightPercentage = 1 - (distance/100f);
		float widthPercentage = 1 - (distance/75f);
		float paperRollPercentage = 1 - (distance / 25f);

		circle.SetDimensions((int)(this.width*widthPercentage), (int)(this.height*heightPercentage));

		paperMask.transform.position = new Vector2 (paperMask.transform.position.x, (this.paperMaskY) * -distance * 1.45f);

		if (speedBar != null) {  // Eliminate If
			speedBar.value = (speedBar.value + ((int)(scrollableObject.rigidbody.velocity.y * - 1f)) / 100f - 0.01f);

			if (speedBar.value > 0f && speedBar.value <= 0.5f) {
				byte greenColorValue = (byte)(255*(speedBar.value*2));
				speedBar.foregroundWidget.color = new Color32 (255, greenColorValue, 0, 255); 
			}

			if (speedBar.value > 0.5f) {
				byte redColorValue = (byte)(255 - 255*((speedBar.value-0.5)*2));
				speedBar.foregroundWidget.color = new Color32 (redColorValue, 255, 0, 255);
			}

			// Old speed bar color formulas
//
//			if (speedBar.value > 0f && speedBar.value <= 0.3f) {
//					speedBar.foregroundWidget.color = new Color32 (192, 57, 43, 255);
//			}
//
//			if (speedBar.value > 0.3f && speedBar.value <= 0.6f) {
//					speedBar.foregroundWidget.color = new Color32 (243, 156, 18, 255);
//			}
//
//			if (speedBar.value > 0.6f) {
//					speedBar.foregroundWidget.color = new Color32 (46, 204, 113, 255);
//			}
		}

		if (!touchEnabled){
			return;
		}

		if (heightPercentage < 0.55f) {
			paperRoll.rigidbody2D.isKinematic = false;
			timer.Pause();

//			if (paperRoll.transform.position.y < -3) {
//				EndOfGameWindow.Show ("UnRollGameScene", 2312, 9291);   // End Game Window
//				touchEnabled = false;
//			}
		}

		//circle.SetDimensions ((int)-scrollableObject.transform.position.y, (int)-scrollableObject.transform.position.y);

#if UNITY_EDITOR
		worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);

		if(Input.GetMouseButtonDown(0)){ //Check if left button is pressed
			touchStart(worldPoint);
		}
		else if(Input.GetMouseButton(0)){
			touchMove(worldPoint);
		}

		if(Input.GetMouseButtonUp(0)){
			touchEnd();
		}

#endif

#if UNITY_ANDROID || UNITY_IPHONE
		var tapCount = Input.touchCount;
		if (tapCount > 0){
			for ( var i = 0 ; i < tapCount ; i++ ) {
				var userTouch = Input.GetTouch(i);
				worldPoint = Camera.main.ScreenToWorldPoint(userTouch.position);

				if (userTouch.phase == TouchPhase.Began){
					touchStart(worldPoint);
				}


				if (userTouch.phase == TouchPhase.Moved){
					touchMove(worldPoint);

				}

				if (userTouch.phase == TouchPhase.Ended){
					touchEnd();
				}
					
			}
		}
#endif
	
	}

	void touchStart(Vector3 point){
		initialTouch = point;
		Utils.Debug.Log ("DragManager", "Initial touch at: "+point, debug);
	}

	void touchMove(Vector3 point){
		Vector3 delta = initialTouch - point;
		deltaForceY += delta.y;
		movingDeltaTime += Time.deltaTime;


		HandleDrag(delta);
		Utils.Debug.Log ("DragManager", "Moving from "+initialTouch+" to "+point+" (Delta: "+delta+")", debug);
		initialTouch = point;
	
	}

	void touchEnd(){
		if(deltaForceY > 0.05f)
			ApplyVelocity(deltaForceY);
		
		deltaForceY =0;
		movingDeltaTime = 0;
	}

	void HandleDrag(Vector3 delta){
		Vector2 clickPosition = new Vector2(scrollableObject.transform.position.x,scrollableObject.transform.position.y-delta.y);
		scrollableObject.transform.position = clickPosition;
	}


	void ApplyVelocity (float deltaY){
		float velocity = (deltaY/movingDeltaTime);
		scrollableObject.rigidbody.velocity = new Vector3 (scrollableObject.rigidbody.velocity.x, -velocity, 0f);
	}
}
